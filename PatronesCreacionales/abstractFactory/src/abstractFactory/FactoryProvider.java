package abstractFactory;

public class FactoryProvider {
	
	private FactoryProvider() {}

	public static AbstractFactory<?> getFactory(String tipoFactory) {
		switch (tipoFactory) {
		case "TARJETA":
			return new TarjetaFactory();
		case "METODO_PAGO":
			return new MetodoPagoFactory();
		default:
			return null;
		}
	}
}
