package abstractFactory;

public class Main {

	public static void main(String[] args) {
		AbstractFactory<Tarjeta> abstractFactoryTarjeta = (TarjetaFactory) FactoryProvider.getFactory("TARJETA");
		Tarjeta tarjeta = abstractFactoryTarjeta.crear("VISA");

		AbstractFactory<MetodoPago> abstractFactoryMetodoPago = (MetodoPagoFactory) FactoryProvider
				.getFactory("METODO_PAGO");
		MetodoPago metodoPago = abstractFactoryMetodoPago.crear("DEBITO");

		System.out.println("Una tarjeta de tipo " + tarjeta.getTipoTarjeta() + " con el método de pago: "
				+ metodoPago.realizarPago());
	}

}
