package abstractFactory;

public class MetodoPagoCredito implements MetodoPago {

	@Override
	public String realizarPago() {
		return "Pago a crédito";
	}

}
