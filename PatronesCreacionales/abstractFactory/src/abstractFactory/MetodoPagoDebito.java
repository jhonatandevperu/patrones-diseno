package abstractFactory;

public class MetodoPagoDebito implements MetodoPago {

	@Override
	public String realizarPago() {
		return "Pago a Debito";
	}

}
