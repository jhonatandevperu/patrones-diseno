package abstractFactory;

public class MetodoPagoFactory implements AbstractFactory<MetodoPago> {

	@Override
	public MetodoPago crear(String tipo) {
		switch (tipo) {
		case "DEBITO":
			return new MetodoPagoDebito();
		case "CREDITO":
			return new MetodoPagoCredito();
		default:
			return null;
		}
	}

}
