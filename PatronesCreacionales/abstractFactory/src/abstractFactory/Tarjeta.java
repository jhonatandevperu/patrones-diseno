package abstractFactory;

public interface Tarjeta {

	String getTipoTarjeta();

	String getNumeroTarjeta();
}
