package abstractFactory;

public class TarjetaFactory implements AbstractFactory<Tarjeta> {

	@Override
	public Tarjeta crear(String tipo) {

		switch (tipo) {
		case "VISA":
			return new TarjetaVisa();
		case "MASTERCARD":
			return new TarjetaMasterCard();
		default:
			return null;
		}
	}

}
