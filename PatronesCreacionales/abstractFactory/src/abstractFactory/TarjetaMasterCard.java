package abstractFactory;

public class TarjetaMasterCard implements Tarjeta {

	@Override
	public String getTipoTarjeta() {
		return "MASTERCARD";
	}

	@Override
	public String getNumeroTarjeta() {
		return "0000 0000 MAST CARD";
	}

}
