package abstractFactory;

public class TarjetaVisa implements Tarjeta {

	@Override
	public String getTipoTarjeta() {
		return "VISA";
	}

	@Override
	public String getNumeroTarjeta() {
		return "0000 0000 0000 0VISA";
	}

}
