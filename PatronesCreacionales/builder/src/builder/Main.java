package builder;

public class Main {

	public static void main(String[] args) {
		Tarjeta tarjetaDebito = new Tarjeta.TarjetaBuilder("VISA", "0000 1111 2222 VISA").credito(false).build();
		Tarjeta tarjetaCredito = new Tarjeta.TarjetaBuilder("VISA", "0000 1111 2222 VISA").expires(2050).credito(true)
				.nombre("Roberto").build();

		System.out.println("tarjetaDebito: " + tarjetaDebito);
		System.out.println("tarjetaCredito: " + tarjetaCredito);
	}

}
