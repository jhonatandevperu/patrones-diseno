package builder;

public class Tarjeta {
	private final String tipo;
	private final String numero;
	private final String nombre;
	private final int expires;
	private final boolean credito;

	public Tarjeta(TarjetaBuilder builder) {
		this.tipo = builder.tipo;
		this.numero = builder.numero;
		this.nombre = builder.nombre;
		this.expires = builder.expires;
		this.credito = builder.credito;
	}

	public String getTipo() {
		return tipo;
	}

	public String getNumero() {
		return numero;
	}

	public String getNombre() {
		return nombre;
	}

	public int getExpires() {
		return expires;
	}

	public boolean isCredito() {
		return credito;
	}
	
	@Override
	public String toString() {
		return "Tarjeta [tipo=" + tipo + ", numero=" + numero + ", nombre=" + nombre + ", expires=" + expires
				+ ", credito=" + credito + "]";
	}

	public static class TarjetaBuilder {
		private String tipo;
		private String numero;
		private String nombre;
		private int expires;
		private boolean credito;

		public TarjetaBuilder(String tipo, String numero) {
			this.tipo = tipo;
			this.numero = numero;
		}

		public TarjetaBuilder nombre(String nombre) {
			this.nombre = nombre;
			return this;
		}

		public TarjetaBuilder expires(int expires) {
			this.expires = expires;
			return this;
		}

		public TarjetaBuilder credito(boolean credito) {
			this.credito = credito;
			return this;
		}

		public Tarjeta build() {
			return new Tarjeta(this);
		}

	}

}
