package factoryMethod;

public class Main {

	public static void main(String[] args) {
		MetodoPago metodoPago = MetodoPagoFactory.buildMetodoPago(TipoMetodoPago.VISA);
		metodoPago.realizarPago();
	}

}
