package factoryMethod;

public class MetodoPagoFactory {
	
	private MetodoPagoFactory(){}

	public static MetodoPago buildMetodoPago(TipoMetodoPago tipoMetodoPago) {
		switch (tipoMetodoPago) {
		case VISA:
			return new MetodoPagoVisa();
		case MASTERCARD:
			return new MetodoPagoMasterCard();
		default:
			return new MetodoPagoMasterCard();
		}
	}
	
}
