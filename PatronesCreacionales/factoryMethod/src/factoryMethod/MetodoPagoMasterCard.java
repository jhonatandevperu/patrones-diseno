package factoryMethod;

public class MetodoPagoMasterCard implements MetodoPago {

	@Override
	public void realizarPago() {
		System.out.println("Realizando pago por tarjeta MasterCard.");
	}

}
