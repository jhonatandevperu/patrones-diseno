package factoryMethod;

public class MetodoPagoVisa implements MetodoPago {

	@Override
	public void realizarPago() {
		System.out.println("Realizando pago por tarjeta VISA.");
	}

}
